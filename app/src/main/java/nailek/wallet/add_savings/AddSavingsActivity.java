package nailek.wallet.add_savings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import nailek.wallet.R;
import nailek.wallet.UserPrefs;
import nailek.wallet.main.MainActivity;

public class AddSavingsActivity extends AppCompatActivity implements AddSavingsContract.View {

    private AddSavingsContract.UserActionsListener mActionsListener;

    private EditText name;
    private EditText saveGoal;
    private EditText savedMoney;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_savings);

        mActionsListener = new AddSavingsPresenter(getSharedPreferences(UserPrefs.FILE, MODE_PRIVATE), this);

        initToolbar();
        initUI();
    }

    private void initToolbar() {
        TextView appBar = (TextView) findViewById(R.id.appbar_title);
        appBar.setText(R.string.add_savings_jar);

        initBackButton();
    }

    private void initBackButton() {
        ImageView backButton = (ImageView) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void initUI() {
        name = (EditText) findViewById(R.id.name);
        saveGoal = (EditText) findViewById(R.id.saveGoal);
        savedMoney = (EditText) findViewById(R.id.savedMoney);
        initCreateSavingsJar();
    }

    private void initCreateSavingsJar() {
        Button doneButton = (Button) findViewById(R.id.done);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (isEditTextNotEmpty(name) && isEditTextNotEmpty(saveGoal) && isEditTextNotEmpty(savedMoney)) {
                    String nameText = name.getText().toString();
                    float saveGoalFloat = Float.parseFloat(saveGoal.getText().toString());
                    float savedMoneyFloat = Float.parseFloat(savedMoney.getText().toString());

                    mActionsListener.addNewSavingsJar(nameText, saveGoalFloat, savedMoneyFloat);
                }
            }
        });
    }

    private boolean isEditTextNotEmpty(EditText view) {
        if (view.getText().toString().equals("")) {
            view.setError("Can't be empty!");
            return false;
        }
        return true;
    }

    @Override
    public void finishActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
