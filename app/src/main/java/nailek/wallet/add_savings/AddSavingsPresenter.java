package nailek.wallet.add_savings;

import android.content.SharedPreferences;

import nailek.wallet.UserPrefs;
import nailek.wallet.model.SavingsJar;

class AddSavingsPresenter implements AddSavingsContract.UserActionsListener {

    private UserPrefs userPrefs;
    private AddSavingsContract.View addSavingsView;

    AddSavingsPresenter(SharedPreferences preferences, AddSavingsContract.View addSavingsView) {
        this.userPrefs = new UserPrefs(preferences);
        this.addSavingsView = addSavingsView;
    }

    @Override
    public void addNewSavingsJar(String name, float saveGoal, float savedMoney) {
        SavingsJar savingsJar = new SavingsJar(name, saveGoal, savedMoney);
        userPrefs.saveNewSavingsJar(savingsJar);
        addSavingsView.finishActivity();
    }
}
