package nailek.wallet.add_savings;

interface AddSavingsContract {

    interface View {
        void finishActivity();
    }

    interface UserActionsListener {
        void addNewSavingsJar(String name, float saveGoal, float savedMoney);
    }
}
