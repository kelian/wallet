package nailek.wallet;

import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

import nailek.wallet.model.SavingsJar;
import nailek.wallet.model.Transaction;

public class UserPrefs {

    public static final String FILE = "user_prefs";

    public static final String BALANCE = "balance";
    private static final String TRANSACTION_LIST = "transaction_list";
    public static final String SAVINGS_JAR_LIST = "savings_jar_list";

    private SharedPreferences prefs;
    private Gson gson;

    public UserPrefs(SharedPreferences prefs) {
        this.prefs = prefs;
        gson = new Gson();
    }

    public float getBalance() {
        return prefs.getFloat(BALANCE, 0);
    }

    public void setBalance(float balance) {
        SharedPreferences.Editor editor = prefs.edit();
        editor.putFloat(BALANCE, balance);
        editor.apply();
    }

    public List<Transaction> getTransactionList() {
        String transactionsJson = prefs.getString(TRANSACTION_LIST, null);
        if (transactionsJson != null )
            return gson.fromJson(transactionsJson, new TypeToken<List<Transaction>>() {}.getType());
        return new ArrayList<>();
    }

    private void saveTransactionList(List<Transaction> transactionList) {
        String transactionsJson = gson.toJson(transactionList);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(TRANSACTION_LIST, transactionsJson);
        editor.apply();
    }

    public void saveTransaction(Transaction transaction) {
        List<Transaction> transactionList = getTransactionList();
        transactionList.add(0, transaction);
        saveTransactionList(transactionList);
        calculateBalance(transaction);
    }

    public void deleteTransaction(Transaction oldTransaction) {
        List<Transaction> transactionList = getTransactionList();
        for (Transaction transaction : transactionList) {
            if (transaction.getId().equals(oldTransaction.getId())) {
                transactionList.remove(transaction);
                removeFromBalance(oldTransaction);
                break;
            }
        }
        saveTransactionList(transactionList);
    }

    public List<SavingsJar> getSavingsJarList() {
        String savingsJarJson = prefs.getString(SAVINGS_JAR_LIST, null);
        if (savingsJarJson != null)
            return gson.fromJson(savingsJarJson, new TypeToken<List<SavingsJar>>() {}.getType());
        return new ArrayList<>();
    }

    public void saveSavingsJarList(List<SavingsJar> savingsJarList) {
        String savingsJarJson = gson.toJson(savingsJarList);
        SharedPreferences.Editor editor = prefs.edit();
        editor.putString(SAVINGS_JAR_LIST, savingsJarJson);
        editor.apply();
    }

    public void saveNewSavingsJar(SavingsJar savingsJar) {
        List<SavingsJar> savingsJarList = getSavingsJarList();
        savingsJarList.add(savingsJar);
        saveSavingsJarList(savingsJarList);
    }

    private void saveExistingJar(SavingsJar existingJar) {
        List<SavingsJar> savingsJarList = getSavingsJarList();

        ListIterator<SavingsJar> iterator = savingsJarList.listIterator();
        while (iterator.hasNext()) {
            if (iterator.next().getId().equals(existingJar.getId())) {
                iterator.set(existingJar);
                break;
            }
        }
        saveSavingsJarList(savingsJarList);
    }

    public void deleteSavingsJar(SavingsJar oldSavingsJar) {
        List<SavingsJar> savingsJarList = getSavingsJarList();
        for (SavingsJar savingsJar : savingsJarList) {
            if (savingsJar.getId().equals(oldSavingsJar.getId())) {
                savingsJarList.remove(savingsJar);
                break;
            }
        }
        saveSavingsJarList(savingsJarList);
    }

    private void addMoneyToSavingJar(String savingsJarId, float moneyAmount) {
        SavingsJar savingsJar = getSavingJarById(savingsJarId);
        if (savingsJar != null) {
            savingsJar.addMoneyToSavings(moneyAmount);
            saveExistingJar(savingsJar);
        }
    }

    private void removeMoneyFromSavingJar(String savingsJarId, float moneyAmount) {
        SavingsJar savingsJar = getSavingJarById(savingsJarId);
        if (savingsJar != null) {
            savingsJar.removeMoneyFromSavings(moneyAmount);
            saveExistingJar(savingsJar);
        }
    }

    private SavingsJar getSavingJarById(String savingJarId) {
        List<SavingsJar> savingsJarList = getSavingsJarList();
        for (SavingsJar savingsJar: savingsJarList) {
            if (savingsJar.getId().equals(savingJarId))
                return savingsJar;
        }
        return null;
    }

    public String getBalanceString() {
        return String.format("%.02f", getBalance()) + "€";
    }

    private void calculateBalance(Transaction transaction) {
        float balance = getBalance();
        switch (transaction.getTransactionMode()) {
            case EXPENSE:
                balance = balance - transaction.getMoneyAmount();
                break;
            case INCOME:
                balance = balance + transaction.getMoneyAmount();
                break;
            case SAVINGS:
                balance = balance - transaction.getMoneyAmount();
                addMoneyToSavingJar(transaction.getSavingsJarId(), transaction.getMoneyAmount());
        }
        setBalance(balance);
    }

    private void removeFromBalance(Transaction transaction) {
        float balance = getBalance();
        switch (transaction.getTransactionMode()) {
            case EXPENSE:
                balance = balance + transaction.getMoneyAmount();
                break;
            case INCOME:
                balance = balance - transaction.getMoneyAmount();
                break;
            case SAVINGS:
                balance = balance + transaction.getMoneyAmount();
                removeMoneyFromSavingJar(transaction.getSavingsJarId(), transaction.getMoneyAmount());
        }
        setBalance(balance);
    }
}
