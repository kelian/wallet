package nailek.wallet.savings;

import android.content.SharedPreferences;

import nailek.wallet.UserPrefs;
import nailek.wallet.model.SavingsJar;

public class SavingsPresenter implements SavingsContract.UserActionsListener {

    private UserPrefs prefs;

    public SavingsPresenter(SharedPreferences preferences) {
        this.prefs = new UserPrefs(preferences);
    }

    @Override
    public void deleteSavingsJar(SavingsJar savingsJar) {
        prefs.deleteSavingsJar(savingsJar);
    }
}
