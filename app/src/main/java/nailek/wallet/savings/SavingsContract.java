package nailek.wallet.savings;

import nailek.wallet.model.SavingsJar;

public interface SavingsContract {

    interface UserActionsListener {
        void deleteSavingsJar(SavingsJar savingsJar);
    }
}
