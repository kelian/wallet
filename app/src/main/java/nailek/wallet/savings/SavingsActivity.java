package nailek.wallet.savings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nailek.wallet.R;
import nailek.wallet.UserPrefs;
import nailek.wallet.add_savings.AddSavingsActivity;
import nailek.wallet.main.MainSavingsAdapter;
import nailek.wallet.model.SavingsJar;
import nailek.wallet.model.SavingsListener;

public class SavingsActivity extends AppCompatActivity {

    private SavingsContract.UserActionsListener mActionsListener;

    private SavingsListener savingsListener = new SavingsListener() {
        @Override
        public void deleteSavingsJar(SavingsJar savingsJar) {
            mActionsListener.deleteSavingsJar(savingsJar);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_savings);

        mActionsListener = new SavingsPresenter(getSharedPreferences(UserPrefs.FILE, MODE_PRIVATE));

        initToolbar();
        initSavings();
        initBackButton();
    }

    private void initToolbar() {
        TextView appBar = (TextView) findViewById(R.id.appbar_title);
        appBar.setText(R.string.savings_jars);
    }

    private void initSavings() {
        List<SavingsJar> savingsJarList  = new UserPrefs(getSharedPreferences(UserPrefs.FILE, MODE_PRIVATE)).getSavingsJarList();
        MainSavingsAdapter savingsAdapter =
                new MainSavingsAdapter(getBaseContext(), savingsJarList, savingsListener);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview_savings);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
        recyclerView.setAdapter(savingsAdapter);

        Button addSavingsJar = (Button) findViewById(R.id.add);
        addSavingsJar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(SavingsActivity.this, AddSavingsActivity.class));
            }
        });
    }

    private void initBackButton() {
        ImageView backButton = (ImageView) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


}
