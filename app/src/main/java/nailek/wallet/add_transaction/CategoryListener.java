package nailek.wallet.add_transaction;

import nailek.wallet.model.Category;

public interface CategoryListener {
    void selectCategory(Category category);
}
