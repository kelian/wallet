package nailek.wallet.add_transaction;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.Calendar;

import nailek.wallet.R;
import nailek.wallet.UserPrefs;
import nailek.wallet.main.MainActivity;
import nailek.wallet.model.Categories;
import nailek.wallet.model.Category;
import nailek.wallet.model.Transaction;
import nailek.wallet.model.TransactionMode;

import static nailek.wallet.utils.Constants.Extra.ACTION_TAG;
import static nailek.wallet.utils.Constants.Extra.CATEGORY_TAG;
import static nailek.wallet.utils.Constants.Extra.TRANSACTION_MODE_TAG;
import static nailek.wallet.utils.Constants.Extra.TRANSACTION_TAG;
import static nailek.wallet.utils.Constants.RequestCode.SELECT_CATEGORY;

public class AddTransactionActivity extends AppCompatActivity implements AddTransactionContract.View,
        DatePickerDialog.OnDateSetListener {

    public enum MoneyAction {
        CREATE,
        EDIT
    }

    private AddTransactionContract.UserActionsListener mActionsListener;

    private MoneyAction moneyAction;
    private Transaction previousTransaction;
    private Transaction transaction = new Transaction();

    private TextView appBarTitle;
    private TextView dateView;
    private TextView categoryView;
    private TextView savingJarView;

    private ImageView categoryIcon;

    private EditText description;
    private EditText moneyAmount;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mActionsListener = new AddTransactionPresenter(getBaseContext(),
                new UserPrefs(getSharedPreferences(UserPrefs.FILE, MODE_PRIVATE)), this);

        Intent intent = getIntent();
        if (intent.hasExtra(ACTION_TAG))
            moneyAction = (MoneyAction) intent.getSerializableExtra(ACTION_TAG);

        if (intent.hasExtra(TRANSACTION_TAG)) {
            previousTransaction = intent.getParcelableExtra(TRANSACTION_TAG);
            transaction = getNewUnlinkedTransaction(previousTransaction);
        }

        if (intent.hasExtra(TRANSACTION_MODE_TAG))
            transaction.setTransactionMode((TransactionMode) intent.getSerializableExtra(TRANSACTION_MODE_TAG));

        initUI();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == SELECT_CATEGORY) {
            if (resultCode == RESULT_OK) {
                transaction.setCategory((Category) data.getParcelableExtra(CATEGORY_TAG));
                mActionsListener.setCategoryInfo(transaction.getCategory());
            }
        }
    }

    // method for not creating a link between previousTransaction and transaction
    private Transaction getNewUnlinkedTransaction(Transaction previousTransaction) {
        Transaction transaction = new Transaction(
                previousTransaction.getTransactionMode(),
                previousTransaction.getMoneyAmount(),
                previousTransaction.getCategory(),
                previousTransaction.getDate(),
                previousTransaction.getSavingsJarId(),
                previousTransaction.getDescription()
        );
        return transaction;
    }

    private void initUI() {
        setContentView(getLayoutId(transaction.getTransactionMode()));

        appBarTitle = (TextView) findViewById(R.id.appbar_title);
        mActionsListener.showTitle(transaction.getTransactionMode(), moneyAction);

        initSelectCategory();
        initDate();
        initSavingJar();

        description = (EditText) findViewById(R.id.description);
        moneyAmount = (EditText) findViewById(R.id.money);

        initData();
        initSubmitButton();
        initBackButton();
    }

    private int getLayoutId(TransactionMode mode) {
        if (mode == TransactionMode.INCOME || mode == TransactionMode.EXPENSE) {
            return R.layout.activity_add_transaction;
        } else {
            return R.layout.activity_add_transaction_savings;
        }
    }

    private void initSelectCategory() {
        categoryIcon = (ImageView) findViewById(R.id.category_icon);
        categoryView = (TextView) findViewById(R.id.category_text);

        FrameLayout categoryBox = (FrameLayout) findViewById(R.id.select_category);
        categoryBox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openSelectCategory();
            }
        });

        mActionsListener.setCategoryInfo(transaction.getCategory());
    }

    private void initDate() {
        if (moneyAction == MoneyAction.CREATE)
            transaction.setDate(Calendar.getInstance());

        dateView = (TextView) findViewById(R.id.date);
        mActionsListener.getDateAsString(transaction.getDate());

        FrameLayout selectDate = (FrameLayout) findViewById(R.id.select_date);
        selectDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDateDialog().show();
            }
        });
    }

    private void initSavingJar() {
        if (transaction.getTransactionMode() == TransactionMode.SAVINGS) {
            savingJarView = (TextView) findViewById(R.id.savingjar);
            FrameLayout savingJarBox = (FrameLayout) findViewById(R.id.select_savings_jar);

            if (mActionsListener.isSavingsJarEmpty()) {
                savingJarView.setText(getString(R.string.no_savings_jars));
                savingJarBox.setClickable(false);
            } else {

                if (transaction.getSavingsJarId() != null) {
                    mActionsListener.getSavingJarName(transaction.getSavingsJarId());
                }

                savingJarBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getSavingsDialog().show();
                    }
                });
            }
        }
    }

    private void initSubmitButton() {
        Button doneButton = (Button) findViewById(R.id.submit);
        doneButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkIfMoneyAmountEmpty(moneyAmount))
                    moneyAmount.setError(getString(R.string.error_money));
                else
                    saveTransaction();
            }
        });
    }

    private void initBackButton() {
        ImageView backButton = (ImageView) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    @Override
    public void setViewTitle(String title) {
        appBarTitle.setText(title);
    }

    public void setCategoryView(String text) {
        categoryView.setText(text);
    }

    @Override
    public void setCategoryImage(int drawableId) {
        categoryIcon.setImageDrawable(ContextCompat.getDrawable(getBaseContext(), drawableId));
    }

    @Override
    public void setDateText(String dateText) {
        dateView.setText(dateText);
    }

    @Override
    public void setSavingJarView(String text) {
        savingJarView.setText(text);
    }

    private void openSelectCategory() {
        Intent selectCategoryIntent = new Intent(AddTransactionActivity.this, AddTransactionCategory.class);
        selectCategoryIntent.putExtra(CATEGORY_TAG, transaction.getCategory());
        startActivityForResult(selectCategoryIntent, SELECT_CATEGORY);
    }

    public Dialog getDateDialog() {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(this, this, year, month, day);
    }

    public Dialog getSavingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder
                .setTitle(R.string.savings_jar)
                .setItems(mActionsListener.getSavingsJarArray(), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int position) {
                        transaction.setSavingsJarId(mActionsListener.getSavingsJarIdByPosition(position));
                        mActionsListener.getSavingJarName(transaction.getSavingsJarId());
                    }
                });

        return builder.create();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        Calendar calendar = transaction.getDate();
        calendar.set(Calendar.YEAR, year);
        calendar.set(Calendar.MONTH, month);
        calendar.set(Calendar.DAY_OF_MONTH, day);
        transaction.setDate(calendar);
        mActionsListener.getDateAsString(calendar);
    }

    private void initData() {
        if (moneyAction == MoneyAction.EDIT) {
            moneyAmount.setText(String.valueOf(transaction.getMoneyAmount()));
            description.setText(String.valueOf(transaction.getDescription()));
        }
    }

    private boolean checkIfMoneyAmountEmpty(EditText moneyAmount) {
        return String.valueOf(moneyAmount.getText()).equals("");
    }

    private void saveTransaction() {
        transaction.setMoneyAmount(Float.parseFloat(moneyAmount.getText().toString()));
        transaction.setDescription(description.getText().toString());
        checkTransactionCategory();

        if (moneyAction == MoneyAction.EDIT)
            mActionsListener.removePreviousTransaction(previousTransaction);

        createTransaction();
    }

    private void checkTransactionCategory() {
        if (transaction.getCategory() == null) {
            transaction.setCategory(new Categories().getDefaultCategory());
        }
    }

    private void createTransaction() {
        mActionsListener.addNewTransaction(transaction);
    }

    @Override
    public void finishActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}

