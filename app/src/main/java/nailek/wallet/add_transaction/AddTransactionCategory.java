package nailek.wallet.add_transaction;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.GridView;
import android.widget.ImageView;

import nailek.wallet.R;
import nailek.wallet.model.Category;

import static nailek.wallet.utils.Constants.Extra.CATEGORY_TAG;

public class AddTransactionCategory extends AppCompatActivity {

    public Category category;

    private CategoryListener categoryListener = new CategoryListener() {
        @Override
        public void selectCategory(Category category) {
            selectedNewCategory(category);
        }
    };

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_transaction_category);

        Intent intent = getIntent();
        if (intent.hasExtra(CATEGORY_TAG))
            category = (Category) intent.getSerializableExtra(CATEGORY_TAG);

        initBackButton();
        initGridView();
    }

    private void initBackButton() {
        ImageView backButton = (ImageView) findViewById(R.id.back);
        backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedNewCategory(category);
            }
        });
    }

    private void initGridView() {
        GridView gridView = (GridView) findViewById(R.id.gridview);
        gridView.setAdapter(new GridViewAdapter(this, categoryListener));
    }

    private void selectedNewCategory(Category newCategory) {
        Intent intent = new Intent();
        intent.putExtra(CATEGORY_TAG, newCategory);
        setResult(RESULT_OK, intent);
        finish();
    }
}
