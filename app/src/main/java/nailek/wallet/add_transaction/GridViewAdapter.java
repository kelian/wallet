package nailek.wallet.add_transaction;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import nailek.wallet.R;
import nailek.wallet.model.Categories;
import nailek.wallet.model.Category;

public class GridViewAdapter extends BaseAdapter {

    private Context context;
    private List<Category> categories = new Categories().getCategories();

    private CategoryListener categoryListener;

    public GridViewAdapter(Context context, CategoryListener categoryListener) {
        this.context = context;
        this.categoryListener = categoryListener;
    }

    @Override
    public int getCount() {
        return categories.size();
    }

    @Override
    public Object getItem(int i) {
        return categories.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final Category category = categories.get(i);

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.item_gridview, null);

            ImageView imageView = (ImageView) view.findViewById(R.id.category_icon);
            TextView textView = (TextView) view.findViewById(R.id.category_text);


            imageView.setImageDrawable(ContextCompat.getDrawable(context, getDrawableId(category.getImagePath())));
            textView.setText(category.getName());
        }

        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                categoryListener.selectCategory(category);
            }
        });

        return view;
    }

    private int getDrawableId(String imagePath) {
        return context.getResources().getIdentifier(
                imagePath, "drawable", context.getPackageName());
    }
}
