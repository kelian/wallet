package nailek.wallet.add_transaction;

import java.util.Calendar;

import nailek.wallet.model.Category;
import nailek.wallet.model.Transaction;
import nailek.wallet.model.TransactionMode;

public interface AddTransactionContract {

    interface View {

        void setViewTitle(String title);

        void setCategoryView(String text);

        void setCategoryImage(int drawableId);

        void setDateText(String dateText);

        void setSavingJarView(String text);

        void finishActivity();
    }

    interface UserActionsListener {

        void showTitle(TransactionMode transactionMode, AddTransactionActivity.MoneyAction action);

        void setCategoryInfo(Category category);

        void getDateAsString(Calendar calendar);

        void removePreviousTransaction(Transaction transaction);

        void addNewTransaction(Transaction transaction);

        String[] getSavingsJarArray();

        boolean isSavingsJarEmpty();

        String getSavingsJarIdByPosition(int position);

        void getSavingJarName(String id);
    }
}
