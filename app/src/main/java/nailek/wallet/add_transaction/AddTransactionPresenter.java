package nailek.wallet.add_transaction;

import android.content.Context;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import nailek.wallet.R;
import nailek.wallet.UserPrefs;
import nailek.wallet.model.Categories;
import nailek.wallet.model.Category;
import nailek.wallet.model.SavingsJar;
import nailek.wallet.model.Transaction;
import nailek.wallet.model.TransactionMode;

public class AddTransactionPresenter implements AddTransactionContract.UserActionsListener {

    private AddTransactionContract.View changeMoneyView;
    private Context context;
    private UserPrefs userPrefs;

    private List<SavingsJar> savingsJarList;
    private Categories categories = new Categories();

    public AddTransactionPresenter(Context context, UserPrefs userPrefs, AddTransactionContract.View changeMoneyView) {
        this.context = context;
        this.userPrefs = userPrefs;
        this.changeMoneyView = changeMoneyView;
        savingsJarList = userPrefs.getSavingsJarList();
    }

    @Override
    public void showTitle(TransactionMode transactionMode, AddTransactionActivity.MoneyAction action) {
        if (action == AddTransactionActivity.MoneyAction.CREATE)
            changeMoneyView.setViewTitle(getAddTitle(transactionMode));
        else
            changeMoneyView.setViewTitle(getEditTitle(transactionMode));
    }

    private String getAddTitle(TransactionMode transactionMode) {
        if (transactionMode == TransactionMode.SAVINGS)
            return context.getString(R.string.add_savings_title);
        else if (transactionMode == TransactionMode.EXPENSE)
            return context.getString(R.string.add_expense_title);
        else
            return context.getString(R.string.add_income_title);
    }

    private String getEditTitle(TransactionMode transactionMode) {
        if (transactionMode == TransactionMode.SAVINGS)
            return context.getString(R.string.edit_savings_title);
        else if (transactionMode == TransactionMode.EXPENSE)
            return context.getString(R.string.edit_expense_title);
        else
            return context.getString(R.string.edit_income_title);
    }

    @Override
    public void setCategoryInfo(Category category) {
        if (category == null) {
            changeMoneyView.setCategoryView(context.getString(R.string.select_category));
            changeMoneyView.setCategoryImage(R.drawable.ic_general);
        } else {
            changeMoneyView.setCategoryView(category.getName());
            changeMoneyView.setCategoryImage(getDrawableId(category.getImagePath()));
        }
    }

    private int getDrawableId(String imagePath) {
        return context.getResources().getIdentifier(
                imagePath, "drawable", context.getPackageName());
    }

    @Override
    public void getDateAsString(Calendar calendar) {
        String dateStr = getDateAsString(calendar.get(Calendar.DAY_OF_MONTH))
                + "/" + getDateAsString(calendar.get(Calendar.MONTH) + 1)
                + "/" + calendar.get(Calendar.YEAR);
        changeMoneyView.setDateText(dateStr);
    }

    private String getDateAsString(int number) {
        if (number < 10) {
            return "0" + number;
        }
        return String.valueOf(number);
    }

    @Override
    public void removePreviousTransaction(Transaction transaction) {
        userPrefs.deleteTransaction(transaction);
    }

    @Override
    public void addNewTransaction(Transaction transaction) {
        userPrefs.saveTransaction(transaction);
        changeMoneyView.finishActivity();
    }

    @Override
    public String[] getSavingsJarArray() {
        List<String> savingJarNames = new ArrayList<>();
        for (SavingsJar savingsJar : savingsJarList)
            savingJarNames.add(savingsJar.getName());

        return savingJarNames.toArray(new String[savingJarNames.size()]);
    }

    @Override
    public boolean isSavingsJarEmpty() {
        return savingsJarList.size() == 0;
    }

    @Override
    public String getSavingsJarIdByPosition(int position) {
        if (savingsJarList.size() > 0)
            return savingsJarList.get(position).getId();
        return null;
    }

    @Override
    public void getSavingJarName(String id) {
        changeMoneyView.setSavingJarView(getSavingsJarById(id).getName());
    }

    public SavingsJar getSavingsJarById(String id) {
        List<SavingsJar> savingsJarList = userPrefs.getSavingsJarList();
        for (SavingsJar savingsJar : savingsJarList) {
            if (savingsJar.getId().equals(id))
                return savingsJar;
        }
        return null;
    }
}
