package nailek.wallet.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Outline;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewOutlineProvider;

public class TriangleView extends View {

    private Paint paint = new Paint();
    private int layoutWidth;

    public TriangleView(Context context) {
        super(context);
        initPaint();
    }

    public TriangleView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initPaint();
    }

    private void initPaint() {
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        int widthMode = MeasureSpec.getMode(widthMeasureSpec);

        int height;
        int heightMode = MeasureSpec.getMode(heightMeasureSpec);
        int heightSize = MeasureSpec.getSize(heightMeasureSpec);

        if (widthMode != MeasureSpec.UNSPECIFIED) {
            layoutWidth = MeasureSpec.getSize(widthMeasureSpec);

            if (heightMode == MeasureSpec.EXACTLY) {
                height = heightSize;
            } else if (heightMode == MeasureSpec.AT_MOST) {
                height = Math.min(layoutWidth, heightSize);
            } else {
                height = layoutWidth;
            }
        } else {
            height = heightSize;
        }

        setMeasuredDimension(layoutWidth, height);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.drawPath(getTrianglePath(), paint);
        setOutlineProvider(viewOutlineProvider);
    }

    private Path getTrianglePath() {
        Point a = new Point(0, 0);
        Point b = new Point(layoutWidth, 0);
        Point c = new Point(layoutWidth / 2, layoutWidth / 2);

        Path path = new Path();
        path.moveTo(a.x, a.y);
        path.lineTo(b.x, b.y);
        path.lineTo(c.x, c.y);
        path.close();

        return path;
    }

    ViewOutlineProvider viewOutlineProvider = new ViewOutlineProvider() {
        @Override
        public void getOutline(View view, Outline outline) {
            outline.setConvexPath(getTrianglePath());
        }
    };


}
