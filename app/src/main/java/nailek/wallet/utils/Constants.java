package nailek.wallet.utils;

public class Constants {

    public static class RequestCode {
        public static final int SELECT_CATEGORY = 0;
    }

    public static class Extra {
        public static final String ACTION_TAG = "action_tag";
        public static final String TRANSACTION_MODE_TAG = "transaction_mode_tag";
        public static final String TRANSACTION_TAG = "transaction_tag";
        public static final String CATEGORY_TAG = "category_tag";
    }
}
