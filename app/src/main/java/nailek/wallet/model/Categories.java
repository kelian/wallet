package nailek.wallet.model;

import java.util.ArrayList;
import java.util.List;

public class Categories {

    private List<Category> categories;

    public Categories() {
        initCategories();
    }

    public List<Category> getCategories() {
        return categories;
    }

    private void initCategories() {
        categories = new ArrayList<>();
        categories.add(new Category("General", "ic_cash"));
        categories.add(new Category("Beauty", "ic_lipstick"));
        categories.add(new Category("Clothes", "ic_tshirt"));
        categories.add(new Category("Food", "ic_food"));
        categories.add(new Category("Fitness", "ic_fitness"));
        categories.add(new Category("Gifts", "ic_gift"));
        categories.add(new Category("Health", "ic_hospital"));
        categories.add(new Category("Savings", "ic_piggy_bank"));
        categories.add(new Category("Transport", "ic_car"));
        categories.add(new Category("Utilities", "ic_home"));
    }

    public String[] getCategoriesArray() {
        List<String> onlyNames = new ArrayList<>();
        for (Category category : categories) {
            onlyNames.add(category.getName());
        }
        return onlyNames.toArray(new String[onlyNames.size()]);
    }

    public Category getDefaultCategory() {
        return getCategories().get(0);
    }
}
