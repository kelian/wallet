package nailek.wallet.model;

import nailek.wallet.utils.Utils;

public class SavingsJar {

    private String name;
    private float saveGoal;
    private float savedMoney;
    private String id;

    public SavingsJar(String name, float saveGoal, float savedMoney) {
        this.name = name;
        this.saveGoal = saveGoal;
        this.savedMoney = savedMoney;
        this.id = Utils.generateUUID();
    }
    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public int getPercentage() {
        return Math.round(savedMoney * 100 / saveGoal);
    }

    public void addMoneyToSavings(float moneyAmount) {
        savedMoney = savedMoney + moneyAmount;
    }

    public void removeMoneyFromSavings(float moneyAmount) {
        savedMoney = savedMoney - moneyAmount;
    }

    public String getPercentageToString() {
        return getPercentage() + "%";
    }

    public String getGoalToString() {
        return savedMoney + "€/" + saveGoal + "€";
    }
}
