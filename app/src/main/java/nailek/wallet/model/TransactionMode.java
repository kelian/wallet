package nailek.wallet.model;

public enum TransactionMode {
    EXPENSE,
    INCOME,
    SAVINGS
}
