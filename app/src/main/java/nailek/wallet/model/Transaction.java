package nailek.wallet.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Calendar;
import java.util.Locale;

import nailek.wallet.utils.Utils;

public class Transaction implements Parcelable{

    private TransactionMode transactionMode;
    private float moneyAmount;
    private Category category;
    private Calendar date;
    private String savingsJarId;
    private String description;
    private String id;

    public Transaction() {
        this.id= Utils.generateUUID();
    }

    public Transaction(TransactionMode transactionMode,
                       float moneyAmount,
                       Category category,
                       Calendar date,
                       String description) {
        this.transactionMode = transactionMode;
        this.moneyAmount = moneyAmount;
        this.category = category;
        this.date = date;
        this.description = description;
        this.id = Utils.generateUUID();
    }

    public Transaction(TransactionMode transactionMode, float moneyAmount, Category category, Calendar date,
                       String savingsJarId, String description) {
        this.transactionMode = transactionMode;
        this.moneyAmount = moneyAmount;
        this.category = category;
        this.date = date;
        this.savingsJarId = savingsJarId;
        this.description = description;
        this.id = Utils.generateUUID();
    }

    public void setTransactionMode(TransactionMode transactionMode) {
        this.transactionMode = transactionMode;
    }

    public void setDate(Calendar date) {
        this.date = date;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public void setSavingsJarId(String savingsJarId) {
        this.savingsJarId = savingsJarId;
    }

    public void setMoneyAmount(float moneyAmount) {
        this.moneyAmount = moneyAmount;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public TransactionMode getTransactionMode() {
        return transactionMode;
    }

    public float getMoneyAmount() {
        return moneyAmount;
    }

    public Category getCategory() {
        return category;
    }

    public Calendar getDate() {
        return date;
    }

    public String getSavingsJarId() {
        return savingsJarId;
    }

    public String getDescription() {
        if (description.equals("")) {
            return "No description";
        }
        return description;
    }

    public String getDateToString() {
        int day = date.get(Calendar.DAY_OF_MONTH);
        int year = date.get(Calendar.YEAR);
        String month = date.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.getDefault());
        return month + " " + day + ", " + year;
    }

    public String getId() {
        return id;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeValue(transactionMode);
        parcel.writeFloat(moneyAmount);
        parcel.writeValue(category);
        parcel.writeValue(date);
        parcel.writeString(savingsJarId);
        parcel.writeString(description);
        parcel.writeString(id);
    }

    protected Transaction(Parcel in) {
        transactionMode = (TransactionMode) in.readValue(TransactionMode.class.getClassLoader());
        moneyAmount = in.readFloat();
        category = (Category) in.readValue(Category.class.getClassLoader());
        date = (Calendar) in.readValue(Calendar.class.getClassLoader());
        savingsJarId = in.readString();
        description = in.readString();
        id = in.readString();
    }

    public static final Creator<Transaction> CREATOR = new Creator<Transaction>() {
        @Override
        public Transaction createFromParcel(Parcel in) {
            return new Transaction(in);
        }

        @Override
        public Transaction[] newArray(int size) {
            return new Transaction[size];
        }
    };
}
