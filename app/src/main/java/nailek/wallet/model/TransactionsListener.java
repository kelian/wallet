package nailek.wallet.model;

public interface TransactionsListener {
    void deleteTransaction(Transaction transaction);
    void editTransaction (Transaction transaction);
}
