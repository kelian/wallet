package nailek.wallet.main;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import java.util.List;

import nailek.wallet.R;
import nailek.wallet.UserPrefs;
import nailek.wallet.add_savings.AddSavingsActivity;
import nailek.wallet.add_transaction.AddTransactionActivity;
import nailek.wallet.model.SavingsJar;
import nailek.wallet.model.SavingsListener;
import nailek.wallet.model.Transaction;
import nailek.wallet.model.TransactionMode;
import nailek.wallet.model.TransactionsListener;

import static nailek.wallet.utils.Constants.Extra.ACTION_TAG;
import static nailek.wallet.utils.Constants.Extra.TRANSACTION_MODE_TAG;
import static nailek.wallet.utils.Constants.Extra.TRANSACTION_TAG;

public class MainActivity extends AppCompatActivity implements MainContract.View {

    private MainContract.UserActionsListener mActionsListener;

    private ActionBarDrawerToggle drawerToggle;

    private TextView balanceView;
    private MainTransactionAdapter transactionAdapter;
    private MainSavingsAdapter savingsAdapter;

    private TransactionsListener transactionsListener = new TransactionsListener() {
        @Override
        public void deleteTransaction(Transaction transaction) {
            mActionsListener.deleteTransaction(transaction);
        }

        @Override
        public void editTransaction(Transaction transaction) {
            mActionsListener.editTransaction(transaction);
        }
    };

    private SavingsListener savingsListener = new SavingsListener() {
        @Override
        public void deleteSavingsJar(SavingsJar savingsJar) {
            mActionsListener.deleteSavingsJar(savingsJar);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mActionsListener = new MainPresenter(getSharedPreferences(UserPrefs.FILE, MODE_PRIVATE), this);

        initToolbar();
        initUI();
    }

    private void initToolbar() {
        TextView appBarTitle = (TextView) findViewById(R.id.appbar_title);
        appBarTitle.setText(getString(R.string.app_name));
    }

    private void initUI() {
        balanceView = (TextView) findViewById(R.id.balance);
        mActionsListener.getBalance();

        initSavings();
        initTransactions();
        initAddTransactionFab();
        initDrawerLayout();
    }

    private void initSavings() {
        mActionsListener.showSavingsList();

        savingsAdapter =
                new MainSavingsAdapter(getBaseContext(), mActionsListener.getSavingsJarsList(), savingsListener);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview_savings);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
        recyclerView.setAdapter(savingsAdapter);

        Button addButton = (Button) findViewById(R.id.add_savings);
        addButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mActionsListener.openAddSavingsView();
            }
        });
    }

    private void initTransactions() {
        transactionAdapter = new MainTransactionAdapter(MainActivity.this,
                mActionsListener.getTransactionsList(), transactionsListener);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview_transactions);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setFocusable(false);
        recyclerView.setAdapter(transactionAdapter);
    }

    private void initAddTransactionFab() {
        final View addTransactionBackground = findViewById(R.id.white_transparent_background);

        final FloatingActionsMenu fabMenu = (FloatingActionsMenu) findViewById(R.id.fab_menu);
        fabMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                addTransactionBackground.setVisibility(View.VISIBLE);
            }

            @Override
            public void onMenuCollapsed() {
                addTransactionBackground.setVisibility(View.GONE);
            }
        });

        FloatingActionButton fabIncome = (FloatingActionButton) findViewById(R.id.income);
        fabIncome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.collapse();
                mActionsListener.openTransactionView(TransactionMode.INCOME);
            }
        });

        FloatingActionButton fabExpense = (FloatingActionButton) findViewById(R.id.expense);
        fabExpense.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.collapse();
                mActionsListener.openTransactionView(TransactionMode.EXPENSE);
            }
        });

        FloatingActionButton fabSavings = (FloatingActionButton) findViewById(R.id.savings);
        fabSavings.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fabMenu.collapse();
                mActionsListener.openTransactionView(TransactionMode.SAVINGS);
            }
        });
    }

    private void initDrawerLayout() {
        MainDrawerLayoutAdapter drawerLayoutAdapter = new MainDrawerLayoutAdapter(MainActivity.this);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recyclerview_drawer);
        recyclerView.setLayoutManager(new LinearLayoutManager(getBaseContext()));
        recyclerView.setAdapter(drawerLayoutAdapter);

        DrawerLayout drawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawerToggle = new ActionBarDrawerToggle(
                this, drawerLayout, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                invalidateOptionsMenu();
            }

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
                invalidateOptionsMenu();
            }
        };
        drawerToggle.setDrawerIndicatorEnabled(true);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void showBalance(String balance) {
        balanceView.setText(balance);
    }

    @Override
    public void showEmptySavingsView() {
        View emptySavings = findViewById(R.id.empty_savings);
        emptySavings.setVisibility(View.VISIBLE);
    }

    @Override
    public void showSavingsRecyclerView() {
        View emptySavings = findViewById(R.id.empty_savings);
        emptySavings.setVisibility(View.GONE);
    }

    @Override
    public void showAddSavingsView() {
        startActivity(new Intent(MainActivity.this, AddSavingsActivity.class));
    }

    @Override
    public void showAddTransactionView(TransactionMode transactionMode) {
        Intent intent = new Intent(MainActivity.this, AddTransactionActivity.class);
        intent.putExtra(ACTION_TAG, AddTransactionActivity.MoneyAction.CREATE);
        intent.putExtra(TRANSACTION_MODE_TAG, transactionMode);
        startActivity(intent);
    }

    @Override
    public void showEditTransactionView(Transaction transaction) {
        Intent intent = new Intent(MainActivity.this, AddTransactionActivity.class);
        intent.putExtra(ACTION_TAG, AddTransactionActivity.MoneyAction.EDIT);
        intent.putExtra(TRANSACTION_TAG, transaction);
        startActivity(intent);
    }

    @Override
    public void refreshSavingsRecyclerView(List<SavingsJar> savingsJarList) {
        savingsAdapter.refreshSavingsJarList(savingsJarList);
    }

    @Override
    public void refreshTransactionRecyclerView(List<Transaction> transactionList) {
        transactionAdapter.refreshTransactionList(transactionList);
    }
}

