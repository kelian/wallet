package nailek.wallet.main;

import java.util.List;

import nailek.wallet.model.SavingsJar;
import nailek.wallet.model.Transaction;
import nailek.wallet.model.TransactionMode;

public interface MainContract {

    interface View {

        void showBalance(String balance);

        void showEmptySavingsView();

        void showSavingsRecyclerView();

        void showAddSavingsView();

        void showAddTransactionView(TransactionMode transactionMode);

        void showEditTransactionView(Transaction transaction);

        void refreshSavingsRecyclerView(List<SavingsJar> savingsJarList);

        void refreshTransactionRecyclerView(List<Transaction> transactionList);
    }

    interface UserActionsListener {

        void getBalance();

        List<Transaction> getTransactionsList();

        List<SavingsJar> getSavingsJarsList();

        void showSavingsList();

        void openAddSavingsView();

        void openTransactionView(TransactionMode transactionMode);

        void editTransaction(Transaction transaction);

        void deleteTransaction(Transaction transaction);

        void deleteSavingsJar(SavingsJar savingsJar);
    }
}
