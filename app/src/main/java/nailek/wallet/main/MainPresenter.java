package nailek.wallet.main;

import android.content.SharedPreferences;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import nailek.wallet.UserPrefs;
import nailek.wallet.model.SavingsJar;
import nailek.wallet.model.Transaction;
import nailek.wallet.model.TransactionMode;

public class MainPresenter implements MainContract.UserActionsListener {

    private MainContract.View mainView;
    private UserPrefs prefs;

    public MainPresenter(SharedPreferences preferences, MainContract.View mainView) {
        this.prefs = new UserPrefs(preferences);
        this.mainView = mainView;
    }

    @Override
    public void getBalance() {
        mainView.showBalance(prefs.getBalanceString());
    }

    @Override
    public List<Transaction> getTransactionsList() {
        List<Transaction> transactionList = prefs.getTransactionList();
        return sortTransactionListByDate(transactionList);
    }

    private List<Transaction> sortTransactionListByDate(List<Transaction> transactions) {
        Collections.sort(transactions, new Comparator<Transaction>() {
            @Override
            public int compare(Transaction transaction, Transaction t1) {
                return t1.getDate().compareTo(transaction.getDate());
            }
        });

        return transactions;
    }

    @Override
    public List<SavingsJar> getSavingsJarsList() {
        return prefs.getSavingsJarList();
    }

    @Override
    public void showSavingsList() {
        if (isSavingsJarEmpty())
            mainView.showEmptySavingsView();
        else
            mainView.showSavingsRecyclerView();
    }

    @Override
    public void openAddSavingsView() {
        mainView.showAddSavingsView();
    }

    @Override
    public void openTransactionView(TransactionMode transactionMode) {
        mainView.showAddTransactionView(transactionMode);
    }

    @Override
    public void editTransaction(Transaction transaction) {
        mainView.showEditTransactionView(transaction);
    }

    @Override
    public void deleteTransaction(Transaction transaction) {
        prefs.deleteTransaction(transaction);
        getBalance();
        mainView.refreshSavingsRecyclerView(prefs.getSavingsJarList());
        mainView.refreshTransactionRecyclerView(prefs.getTransactionList());
    }

    @Override
    public void deleteSavingsJar(SavingsJar savingsJar) {
        prefs.deleteSavingsJar(savingsJar);
        mainView.refreshSavingsRecyclerView(prefs.getSavingsJarList());
    }

    private boolean isSavingsJarEmpty() {
        return prefs.getSavingsJarList().size() == 0;
    }

}
