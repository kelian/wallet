package nailek.wallet.main;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import nailek.wallet.R;
import nailek.wallet.savings.SavingsActivity;

class MainDrawerLayoutAdapter extends RecyclerView.Adapter<MainDrawerLayoutAdapter.ViewHolder> {

    private Context context;
    private List<Pair<String, String>> navigationList = new ArrayList<>();

    MainDrawerLayoutAdapter(Context context) {
        this.context = context;
        initNavigationList();
    }

    private void initNavigationList() {
        navigationList.add(new Pair<String, String>("Savings Jars", "ic_piggy_bank"));
        navigationList.add(new Pair<String, String>("Add something here", "ic_cash"));
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        TextView name;
        ImageView icon;

        ViewHolder(View itemView) {
            super(itemView);
            name = (TextView) itemView.findViewById(R.id.item);
            icon = (ImageView) itemView.findViewById(R.id.item_icon);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            String text = (String) name.getText();
            switch(text) {
                case "Savings Jars":
                    context.startActivity(new Intent(context, SavingsActivity.class));
                    break;
                default:
                    break;
            }

        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_recyclerview_drawerlayout, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Pair<String, String> pair = navigationList.get(position);
        holder.name.setText(pair.first);
        holder.icon.setImageDrawable(ContextCompat.getDrawable(context, getDrawableId(pair.second)));
    }

    @Override
    public int getItemCount() {
        return navigationList.size();
    }

    private int getDrawableId(String drawable) {
        return context.getResources().getIdentifier(drawable, "drawable", context.getPackageName());
    }
}
