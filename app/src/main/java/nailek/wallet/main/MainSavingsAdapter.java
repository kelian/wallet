package nailek.wallet.main;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import nailek.wallet.R;
import nailek.wallet.model.SavingsJar;
import nailek.wallet.model.SavingsListener;

public class MainSavingsAdapter extends RecyclerView.Adapter<MainSavingsAdapter.ViewHolder> {

    private Context context;
    private List<SavingsJar> savingsJars;
    private SavingsListener savingsListener;

    public MainSavingsAdapter(Context context, List<SavingsJar> savingsJars,
                              SavingsListener savingsListener) {
        this.context = context;
        this.savingsJars = savingsJars;
        this.savingsListener = savingsListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ProgressBar progressBar;
        TextView progressBarText;
        TextView name;
        TextView goal;
        ImageView delete;

        ViewHolder(View itemView) {
            super(itemView);
            progressBar = (ProgressBar) itemView.findViewById(R.id.progressBar);
            progressBarText = (TextView) itemView.findViewById(R.id.progressBarText);
            name = (TextView) itemView.findViewById(R.id.name);
            goal = (TextView) itemView.findViewById(R.id.goal);
            delete = (ImageView) itemView.findViewById(R.id.delete);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            changeOptionsVisibility(delete, goal, goal.getVisibility() == View.GONE);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_recyclerview_savingsjar, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final SavingsJar savingsJar = savingsJars.get(position);

        holder.progressBar.setProgress(savingsJar.getPercentage());
        holder.progressBarText.setText(savingsJar.getPercentageToString());
        holder.name.setText(savingsJar.getName());
        holder.goal.setText(savingsJar.getGoalToString());

        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                savingsListener.deleteSavingsJar(savingsJar);
            }
        });
    }

    @Override
    public int getItemCount() {
        return savingsJars.size();
    }

    private void changeOptionsVisibility(ImageView delete, TextView goal, boolean show) {
        goal.setVisibility(show ? View.VISIBLE : View.GONE);
        delete.setVisibility(show ? View.GONE : View.VISIBLE);
    }

    public void refreshSavingsJarList(List<SavingsJar> savingsJarList) {
        this.savingsJars = savingsJarList;
        notifyDataSetChanged();
    }

}
