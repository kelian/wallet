package nailek.wallet.main;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import nailek.wallet.R;
import nailek.wallet.model.Transaction;
import nailek.wallet.model.TransactionMode;
import nailek.wallet.model.TransactionsListener;

class MainTransactionAdapter extends RecyclerView.Adapter<MainTransactionAdapter.ViewHolder> {

    private Context context;
    private List<Transaction> transactions;
    private TransactionsListener transactionsListener;

    MainTransactionAdapter(Context context, List<Transaction> transactions,
                           TransactionsListener transactionsListener) {
        this.context = context;
        this.transactions = transactions;
        this.transactionsListener = transactionsListener;
    }

    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        FrameLayout background;
        ImageView categoryImage;
        TextView date;
        TextView description;
        TextView money;
        LinearLayout options;
        ImageView edit;
        ImageView delete;

        ViewHolder(View itemView) {
            super(itemView);
            background = (FrameLayout) itemView.findViewById(R.id.background);
            categoryImage = (ImageView) itemView.findViewById(R.id.category_image);
            date = (TextView) itemView.findViewById(R.id.date);
            description = (TextView) itemView.findViewById(R.id.description);
            money = (TextView) itemView.findViewById(R.id.money_amount);
            options = (LinearLayout) itemView.findViewById(R.id.options);
            edit = (ImageView) itemView.findViewById(R.id.edit);
            delete = (ImageView) itemView.findViewById(R.id.delete);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            changeOptionsVisibility(options, options.getVisibility() == View.GONE);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(context);
        View itemView = inflater.inflate(R.layout.item_recyclerview_transactions, parent, false);
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Transaction transaction = transactions.get(position);

        changeOptionsVisibility(holder.options, false);

        holder.background.setBackgroundColor(getBackgroundColor(position));
        holder.categoryImage.setImageDrawable(ContextCompat.getDrawable(context, getDrawableId(transaction)));
        holder.date.setText(transaction.getDateToString());
        holder.description.setText(transaction.getDescription());
        holder.money.setText(formatMoneyAmount(holder, transaction.getMoneyAmount(), transaction.getTransactionMode()));
        holder.edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                transactionsListener.editTransaction(transaction);
            }
        });
        holder.delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDeleteConfirmationDialog(transaction).show();
            }
        });
    }

    private int getBackgroundColor(int position) {
        if (position % 2 != 0)
            return ContextCompat.getColor(context, R.color.colorBackground);
        else
            return ContextCompat.getColor(context, android.R.color.white);
    }

    @Override
    public int getItemCount() {
        return transactions.size();
    }

    private String formatMoneyAmount(ViewHolder holder, float moneyAmount, TransactionMode transactionMode) {
        holder.money.setTextColor(ContextCompat.getColor(context, R.color.colorDrawerIcons));

        switch (transactionMode) {
            case INCOME:
                return "+" + String.format("%.02f", moneyAmount) + "€";
            case EXPENSE:
                return "-" + String.format("%.02f", moneyAmount) + "€";
            case SAVINGS:
                return String.format("%.02f", moneyAmount) + "€";
        }
        return "";
    }

    private void changeOptionsVisibility(LinearLayout options, boolean show) {
        options.setVisibility(show ? View.VISIBLE : View.GONE);
    }

    private int getDrawableId(Transaction transaction) {
        return context.getResources().getIdentifier(
                transaction.getCategory().getImagePath(), "drawable", context.getPackageName());
    }

    public void refreshTransactionList(List<Transaction> transactions) {
        this.transactions = transactions;
        notifyDataSetChanged();
    }

    private Dialog getDeleteConfirmationDialog(final Transaction transaction) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder
                .setTitle(R.string.transaction_delete)
                .setMessage(String.format(context.getResources().getString(R.string.transaction_item), transaction.getDescription()))
                .setPositiveButton(R.string.delete, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        transactionsListener.deleteTransaction(transaction);
                    }
                })
                .setNeutralButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
        return builder.create();
    }

}
