package nailek.wallet;

import android.content.SharedPreferences;

import com.google.gson.Gson;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import nailek.wallet.main.MainContract;
import nailek.wallet.main.MainPresenter;
import nailek.wallet.model.SavingsJar;

import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MainPresenterTest {

    @Mock
    private MainContract.View mainView;

    @Mock
    private SharedPreferences mMockSharedPreferences;

    private MainPresenter mainPresenter;

    private Gson gson = new Gson();

    @Before
    public void setupMainPresenter() {
        mainPresenter = new MainPresenter(mMockSharedPreferences, mainView);
    }

    @Test
    public void testGetBalance() {
        mainPresenter.getBalance();

        verify(mainView).showBalance(anyString());
    }

    @Test
    public void testSavingsDataEmpty() {
        mainPresenter.showSavingsList();
        verify(mainView).showEmptySavingsView();
    }

    @Test
    public void testSavingsDataNotEmpty() {
        when(mMockSharedPreferences.getString(eq(UserPrefs.SAVINGS_JAR_LIST), anyString()))
                .thenReturn(getSavingJarString());
        mainPresenter.showSavingsList();
        verify(mainView).showSavingsRecyclerView();
    }

    @Test
    public void testOpenAddSavingsView() {
        mainPresenter.openAddSavingsView();

        verify(mainView).showAddSavingsView();
    }

    @Test
    public void testOpenTransactionView() {
//        mainPresenter.openTransactionView();
//
//        verify(mainView).showAddTransactionView();
    }

    private String getSavingJarString() {
        List<SavingsJar> savingsJarList = new ArrayList<>();
        savingsJarList.add(new SavingsJar("Test", 20, 0));
        return gson.toJson(savingsJarList);
    }

}
